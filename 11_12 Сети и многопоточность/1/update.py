import cv2
import tkinter as tk
from PIL import Image, ImageTk



def update(self):
    try:
        ret, frame = self.stream.read()

        if ret:

            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.photo = ImageTk.PhotoImage(image=Image.fromarray(frame))
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
            if not self.pause:
                self.master.after(15, self.update)
        else:
            self.stream.release()
    except Exception as g:
        print("no stream")
